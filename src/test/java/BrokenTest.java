import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import org.testng.Assert;
import org.testng.annotations.Test;

@Epic("AT")
@Feature("broken test class")

public class BrokenTest {
    @Test(description = "broken test method")
    public void brokenTest(){
        System.out.println("broken test was executed");
        Assert.fail("test was broken");
    }

}
